+++
title = "Language Classification using MFCC"
date = 2021-11-28
[extra]
# Any additional metadata you want to include
+++

# Classifying audio files into their approriate language using MFCC

In this project, our group recorded some pre-defined sentences in audio files using the .mav file extension, turned them into numpy arrays and then used the KNN classifier to classify them into their appropriate languages.

[Github](https://github.com/farazjawedd/Language_classficiation_MFCC)